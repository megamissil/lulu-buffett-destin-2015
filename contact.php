<?php
require_once 'connect.php';
include $_SERVER['DOCUMENT_ROOT'].'/postman/_variables-destin.php';
$contact = pageGet('contact.php');
$contact = (object) $contact;
$cMetaDesc = $contact->title;
$cPageTitle = $contact->title;
$cSEOTitle = '';
$layout = 'home';
include "header.php";
?>

<div class="main" role="main">
    <div class="row">
        <div class="small-12 columns subpage-headline">
            <h1>Contact &nbsp;Us</h1>
        </div>
    </div>
    <div class="row">
    <div class="medium-5 columns purple-panel">
            <?php 

            if ( !empty( $cError ) )
            { 
               
                echo '<p class="error">' . $cError . '</p>';
               
            }
            
            if ( $_SERVER['REQUEST_METHOD'] == 'GET' || !empty( $_POST ) )
            
            {
            
            ?>
            <form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" id="foonster" name="foonster" enctype="multipart/form-data">
                <select name="sendto" id="recipient">
                    <option value="select" selected>*** Select Recipient ***</option>
                    <option value="music">Music Inquiries</option>
                    <option value="jobs">Job Inquiries</option>
                    <option value="destin_general">General Inquiries</option>
                </select>
                <label for="name">Name:</label>
                <input class="txt-wide" type="text" name="name" />
                <label for="email">Email:</label>
                <input class="txt-wide" type="text" name="email" />

                <label for="city">City/State:</label>
                <input class="txt-short" type="text" name="city" />
                <select name="state">
                    <option value="" selected="selected">--</option>
                    <option value="AK">AK</option>
                    <option value="AL">AL</option>
                    <option value="AR">AR</option>
                    <option value="AZ">AZ</option>
                    <option value="CA">CA</option>
                    <option value="CO">CO</option>
                    <option value="CT">CT</option>
                    <option value="DC">DC</option>
                    <option value="DE">DE</option>
                    <option value="FL">FL</option>
                    <option value="GA">GA</option>
                    <option value="HI">HI</option>
                    <option value="IA">IA</option>
                    <option value="ID">ID</option>
                    <option value="IL">IL</option>
                    <option value="IN">IN</option>
                    <option value="KS">KS</option>
                    <option value="KY">KY</option>
                    <option value="LA">LA</option>
                    <option value="MA">MA</option>
                    <option value="MD">MD</option>
                    <option value="ME">ME</option>
                    <option value="MI">MI</option>
                    <option value="MN">MN</option>
                    <option value="MO">MO</option>
                    <option value="MS">MS</option>
                    <option value="MT">MT</option>
                    <option value="NC">NC</option>
                    <option value="ND">ND</option>
                    <option value="NE">NE</option>
                    <option value="NH">NH</option>
                    <option value="NJ">NJ</option>
                    <option value="NM">NM</option>
                    <option value="NV">NV</option>
                    <option value="NY">NY</option>
                    <option value="OH">OH</option>
                    <option value="OK">OK</option>
                    <option value="OR">OR</option>
                    <option value="PA">PA</option>
                    <option value="RI">RI</option>
                    <option value="SC">SC</option>
                    <option value="SD">SD</option>
                    <option value="TN">TN</option>
                    <option value="TX">TX</option>
                    <option value="UT">UT</option>
                    <option value="VA">VA</option>
                    <option value="VT">VT</option>
                    <option value="WA">WA</option>
                    <option value="WI">WI</option>
                    <option value="WV">WV</option>
                    <option value="WY">WY</option>
                </select>

                <label for="comment">Comment:</label>
                <textarea id="comments" name="msg"></textarea>

                <?php

                if ( $lCaptcha ) {

                    echo '<div class="g-recaptcha" data-sitekey="6Lf2uQITAAAAAPHSBevdIFSPe2IX3sauwxLJfq0k"></div>'; 
                }
                ?>
                <a href="javascript:;" onclick="document.getElementById('foonster').submit();" id="btn_submit" class="button">Submit</a>
            </form>
            <?php
            }
            ?>
        </div>
        <div class="medium-6 columns lime-green-panel">
            <?php echo $contact->msg; ?>
        </div>
    </div>
</div>
</div>

<?php
include "footer.php";
?>