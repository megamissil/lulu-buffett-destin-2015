<?php
require_once 'connect.php';
$thought = pageGet('press-releases.php');
$thoughts = pageByCategory('DESTIN', 'ANY', 0, 2500, 'PUBL_DESC');
$cMetaDesc = $thought['title'];
$cPageTitle = $thought['title'];
$cSEOTitle = '';
$layout = 'home';
include "header.php";
?>
<div class="main" role="main">
    <div class="row">
        <div class="small-12 columns subpage-headline">
            <h1><?php $thought['title']; ?></h1>
        </div>
    </div>
    <div class="row">
		<div class="small 12 columns purple-panel text-overlay">

			<article class="single-article">
			    <header>
                    <?php if( isset( $thought['image'] ) )
                    { ?>
                        <img src="<?=$thought['image']; ?>" alt="<?=$thought['title']; ?>" />
                    <?php } ?>
                </header>
                <content>
                    <?=$thought['msg']; ?>
                </content>
                <footer><!-- <a href="<?=$thought['url']; ?>" class="button read-more-cta">View Full Article</a> --></footer>
			</article>
		</div>
	</div>
</div>
<?php
include "footer.php";
