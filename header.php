<?php
    include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
    $cSiteName = 'LuLu\'s Destin';
    $cSiteCity = '';
    $cSiteState = '';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="<?php echo $cMetaDesc; ?>" name="description">
    <meta content="<?php echo $cMetaKW; ?>" name="keywords">
    <title><?php if (!empty($cSEOTitle)) { echo $cSEOTitle; } else if (!empty($cPageTitle)) { ?><?=$cSiteName; ?> – <?php echo $cPageTitle; ?><?php } else { ?><?=$cSiteName; ?> – <?=$cSiteCity; ?>, <?=$cSiteState; ?><?php } ?></title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="bower_components/normalize.css/normalize.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="stylesheets/app.min.css" rel="stylesheet" type="text/css">
    <link href="stylesheets/zeekee-support.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Cabin+Sketch:400,700' rel='stylesheet' type='text/css'>
    <script src="bower_components/modernizr/modernizr.js"></script>
</head>
<body>
<header>
    <div class="row">
        <div class="small-12 columns header-img">
            <img src="img/bg_destin-header-transparent.png" alt="LuLu's at Destin" />
        </div>
    </div>
    
    <!-- MOBILE NAV -->
    <div class="row hide-for-large-up">
      <nav class="top-bar" data-topbar>
        <ul class="title-area">
          <li class="name">
          </li>
          <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        </ul>

        <section class="top-bar-section">
            <ul class="right">
                <li><a href="index.php" title="Home">Home</a></li>
                <li><a href="http://jobs.lulubuffett.com" title="Jobs" target="_blank">Jobs</a></li>
                <li><a href="lulus-thoughts.php" title="Latest Info">Latest Info</a></li>
                <li><a href="contact.php" title="Contact Us">Contact Us</a></li>
            </ul>
        </section>
        </nav>
    </div>
    <!-- END MOBILE NAV -->

    <!-- DESKTOP NAV -->
    <div class="row show-for-large-up" id="top-nav">
       <ul class="small-12 columns">
        <li><a href="index.php" title="Home" class="nav-button-1">Home</a></li>
        <li><a href="http://jobs.lulubuffett.com" title="Jobs" class="nav-button-3" target="_blank">Jobs</a></li>
        <li><a href="lulus-thoughts.php" title="Latest Info" class="nav-button-2">Latest Info</a></li>
        <li><a href="contact.php" title="Contact Us" class="nav-button-4">Contact Us</a></li>
       </ul>
    </div>
    <!-- END DESKTOP NAV -->
</header>
