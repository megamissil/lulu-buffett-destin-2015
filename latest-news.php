<?php
require_once 'connect.php';
$thought = (object) pageCategory('DESTIN');
$thoughts = pageByCategory('DESTIN', 'ANY', 0, 2500, 'PUBL_DESC');
$cMetaDesc = $thought->title;
$cPageTitle = $thought->title;
$cSEOTitle = '';
$layout = 'home';
include "header.php";
?>
<div class="main" role="main">
    <div class="row">
        <div class="small-12 columns subpage-headline">
            <h1>Press</h1>
        </div>
    </div>
    <div class="row">
		<div class="small 12 columns purple-panel">
            <?php foreach( $thoughts as $item )
            { ?>
			<article class="single-article">
			    <header>
                    <?php if( isset( $item['image'] ) )
                    { ?>
                        <img src="<?=$item['image']; ?>" alt="<?=$item['title']; ?>" />
                    <?php } ?>
                </header>
                <content>
                    <h2><?=$item['title']; ?></h2>
                    <?=$item['msg']; ?>
                </content>
                <footer><a href="<?=$item['url']; ?>" class="button read-more-cta">View Full Article</a></footer>
			</article>
            <?php } ?>
		</div>
	</div>
</div>

<?php
include "footer.php";
?>