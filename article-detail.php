<?php
	include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$cMetaDesc = $aOutput['description'];
	$cMetaKW = '';
	$cPageTitle = $aOutput['title'];
	$cSEOTitle = $aOutput['abstract'];
	$layout = 'subpage';
?>

<?php
	include("header.php");
?>

<div id="sub-big-background">
	<div class="graphic-design">
		<section class="sub-header-title">
			<div class="row">
				<div class="medium-2 columns small-centered small-4 hide-for-small">
					<span class="subpage-icon"><img src="img/news-article-header-icon.svg" alt="<?=$aOutput['title']; ?> Article Icon" /></span>
				</div>
			</div><!-- /.row -->
			<div class="row">
				<div class="medium-10 columns medium-centered">
					<h1><?=$aOutput['title']; ?></h1>
				</div>
			</div><!-- /.row -->
		</section>
	</div>
</div>
<main id="container">
	<!-- START PORTFOLIO SECTION -->
	<section class="offwhite">
		<div class="row">
			<div class="medium-8 columns large-4 right">
				<span class='st_sharethis_large' displayText='ShareThis'></span>
				<span class='st_facebook_large' displayText='Facebook'></span>
				<span class='st_twitter_large' displayText='Tweet'></span>
				<span class='st_googleplus_large' displayText='Google +'></span>
				<span class='st_linkedin_large' displayText='LinkedIn'></span>
				<span class='st_pinterest_large' displayText='Pinterest'></span>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<?=$aOutput['msg']; ?>
			</div>
		</div><!-- /.row -->
		<div class="row">
			<div class="medium-6 columns">
				<a href="zeekee-news.php" class="button expand">Return to Zeekee News</a>
			</div>
		</div><!-- /.row -->
	</section>
	<!-- END PORTFOLIO SECTION -->
</main>

<?php
	include("footer.php");
?>
