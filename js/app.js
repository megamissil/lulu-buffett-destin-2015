// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

$('.col-height').matchHeight();

$(".owl-carousel").owlCarousel({
    autoplay:true,
    autoplayHoverPause:true,
    autoplaySpeed:250,
    loop:true,
    center:true,
    items:1,
    pagination:false
});
