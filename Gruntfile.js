module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({

        //Read the package.json (optional)
        pkg: grunt.file.readJSON('package.json'),

        banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
                '* Copyright (c) <%= grunt.template.today("yyyy") %> ',

        // Task configuration.
        play: {
            onSuccess: {
                file: '../MGS-alert-success.mp3'
            }
        },
        concat: {
            options: {
                stripBanners: true
            },
            dist: {
                src: ['js/app.js'],
                dest: 'js/app-lib.js'
            }
        },
        uglify: {
            my_target: {
                files: {
                    'js/app-lib.js': ['js/app.js']
                },
            },
        },
        compass: {
            dev: {
                options: {
                  config: 'config.rb',
                  sassDir: 'scss',
                  cssDir: 'stylesheets'
                },
            },
        },
        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'stylesheets/app.css': 'scss/app.scss'
                }
            },
        },
        cssmin: {
            css: {
                src: 'stylesheets/app.css',
                dest: 'stylesheets/app.min.css'
            }
        },
        watch: {
            compass: {
                files: ['**/*.{scss,sass}'],
                tasks: ['compass:dev', 'play:onSuccess', 'cssmin']
            },
            js: {
                files: ['js/**/*.js'],
                tasks: ['concat']
            }
        }

    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-play');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-compass');

    // Default task
    grunt.registerTask('default', ['compass:dev', 'concat', 'watch', 'cssmin:css']);


};
