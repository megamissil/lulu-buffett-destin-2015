<?php
require_once 'connect.php';
$homepage = pageGet(basename($_SERVER['REQUEST_URI']));;
$homepage = (object) $homepage;
$progressImages = photosAlbum(13);
$progressImages = (object) $progressImages;
$message = pageGet('message.php');
$message = (object) $message;
$cMetaDesc = 'LuLu\'s Destin - Home';
$cPageTitle = 'LuLu\'s Destin - Home';
$cSEOTitle = '';
$layout = 'home';

//
$thought = pageGet('message-from-lulu.php');
$thought = (object) $thought;



include "header.php";
?>
<div class="main" role="main">
<!-- 	<div class="row">
		<div class="small-10 medium-9 small-centered columns">
			<img src="img/img_content-banner.png">
		</div>
	</div> -->
	<div class="row">
		<div class="small 12 columns teal-panel text-overlay">
			<div class="owl-carousel">
                <?php
                    foreach ($progressImages->images as $key => $photo) {
                    	$photo = (object) $photo;
                        echo '<div><img src="' . $photo->image . '" title="' . $photo->msg . '"/><h2 class="text-overlay-container"><span>'.$photo->msg.'</span></h2></div>' . "\n";
	                }
	            ?>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="small-12 medium-8 columns no-left-padding">
			<div class="lime-green-panel">
				<h2><?= $thought->title ?></h2>
				<p><?= $thought->msg ?></p>
				<a href="lulus-thoughts.php" class="button">Read more</a>
			</div>
			<div class="blue-panel">
				<h2>Join our Newsletter!</h2>
				<form action="http://www.lulubuffett.com/addOrUpdateContact.php" id="signup-form">
				<input id="txt_signup"  value="<?=$_POST['email'] ?>" type="text" name="email" placeholder="Email Address" />
				<a href="javascript:;" onclick="document.getElementById('signup-form').submit();" class="button newsletter-cta">Submit</a>
				</form>
			</div>
		</div>
		<div class="small-12 medium-4 columns newsletter-panel">
			<h2>A Message from LuLu</h2>
			<p><?php echo $message->msg ?></p>
		</div>
	</div>
</div>
<?php
	include("footer.php");
?>
