<?php
require_once 'connect.php';
include $_SERVER['DOCUMENT_ROOT'].'/postman/_variables.php';
$contact = pageGet('contact.php');
$contact = (object) $contact;
$cMetaDesc = $contact->title;
$cPageTitle = $contact->title;
$cSEOTitle = '';
$layout = 'home';
include "header.php";
?>

<div class="main" role="main">
    <div class="row">
        <div class="small-12 columns subpage-headline">
            <h1>Thanks</h1>
        </div>
    </div>
    <div class="row">
        <div class="medium-5 columns purple-panel">
            <h2>Thanks for your feedback.</h2>
            <p>We'll be in contact soon.</p>
        </div>
        <div class="medium-6 columns lime-green-panel">
            <?php echo $contact->msg; ?>
        </div>
    </div>
</div>
</div>

<?php
include "footer.php";
?>