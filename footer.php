
<footer>
	<div class="footer-wrapper">
		
		<!-- LOGO -->
		<div class="row">
			<div class="small-6 small-centered medium-4 medium-uncentered columns" id="footer-logo">
				<img src="img/img_lulu-destin-logo.png">
			</div>
			<!-- <div class="medium-3 columns footer-note">
				<h3>A NOTE FROM LULU</h3>
				<p>Come on down and share the good times with me, my friends, and my family. It's a special place, and I invite you to come and enjoy. I guarantee you will have a time you will want to repeat over and over again.<br/>GUMBO LOVE!</p>
				<img src="img/img_signature.png" alt="Lucy Buffett signature">
			</div> -->
			
			<!-- FOOTER LINKS -->
			<div class="medium-4 columns hide-for-small end">
				<div class="row footer-links">
					<ul class="small-12 columns">
						<li><a href="#" title="Home">Home</a></li>
						<li><a href="http://lulubuffett.com" target="_blank">Back to Gulf Shores Site</li>
						<div class=".small-vertical-divider"></div>
						<li><a href="http://shop.lulubuffett.com/" title="Shop Online" target="_blank">Shop Online</a></li>
					</ul>
				</div>
<!-- 				<div class="row">
					<div class="small-6 columns footer-links">
						<h3>INFO</h3>
						<ul>
							<li><a href="http://lulubuffett.com/our-story.php" title="Our Story">Our Story</a></li>
							<li><a href="http://lulubuffett.com/menus.php" title="Menus">Menus</a></li>
							<li><a href="http://lulubuffett.com/directions.php" title="Directions">Directions</a></li>
							<li><a href="http://lulubuffett.com/coconuts.php" title="Coconuts">Coconuts</a></li>
							<li><a href="http://lulubuffett.com/news.php" title="Press">Press</a></li>
							<li><a href="contact.php" title="Got Questions">Got Questions</a></li>
							<li><a href="contact.php" title="Contact">Contact</a></li>
						</ul>
					</div>
					<div class="small-6 columns footer-links">
						<h3>EVENTS</h3>
						<ul>
							<li><a href="http://lulubuffett.com/special-events.php" title="Special Events">Special Events</a></li>
							<li><a href="http://lulubuffett.com/calendar.php" title="Live Music &amp; Calendar">Live Music &amp; Calendar</a></li>
						</ul>
						<h3>JUST FUN</h3>
						<ul>
							<li><a href="http://lulubuffett.com/photo-gallery.php" title="Photo Gallery">Photo Gallery</a></li>
							<li><a href="http://lulubuffett.com/links.php" title="Links">Links</a></li>
						</ul>
					</div>
				</div> -->
			</div>
			<div class="medium-4 columns">
				<ul class="social-icons">
					<li><a href="https://www.facebook.com/pages/LuLus-Destin/334327600065668" title="Facebook" target="_blank"><i class="fa fa-3x fa-facebook-official"></i></a></li>
					<li><a href="https://twitter.com/LuLusDestin" title="Twitter" target="_blank"><i class="fa fa-3x fa-twitter"></i></a></li>
					<li><a href="http://instagram.com/lulusdestin" title="Instagram" target="_blank"><i class="fa fa-3x fa-instagram"></i></a></li>
					<li><a href="https://www.pinterest.com/LuLusDestin" title="Pinterest" target="_blank"><i class="fa fa-3x fa-pinterest"></i></a></li>
				</ul>
			</div>
		</div>
		

		<div class="row">
			<div class="small-12 columns footer-copyright-info">
				<hr>
				<div class="row">
					<div class="text-center medium-4 columns">
						<p>&copy; LuLu's Destin <?=date('Y'); ?></p>
					</div>
					<div class="medium-4 columns hide-for-small">
						<p class="text-center">4607 Legendary Marina Drive <br/> 
						Destin, FL 32541</p>
					</div>
					<div class="text-center medium-4 columns hide-for-small">
						<p>850.710.LULU (5858)</p>
					</div>
				</div><!-- /.row -->
				<div class="row">
					<div class="small-5 medium-2 small-centered columns">
						<a href="https://zeekeeinteractive.com/" target="_blank"><img src="img/zeekee-slug-white-2011.png" alt="Imagined and developed by Zeekee" /></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/fastclick/lib/fastclick.js" type="text/javascript"></script>
<script src="bower_components/foundation/js/foundation.min.js"></script>
<script src="bower_components/owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-5119403-1', 'auto');
  ga('send', 'pageview');

</script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="bower_components/matchHeight/jquery.matchHeight-min.js"></script>
<script src="js/app-lib.js"></script>

</body>
</html>
